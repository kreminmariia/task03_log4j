import org.apache.log4j.Logger;

import java.util.Scanner;

 class BankClient {
    private static final Logger loggerForBankClient = Logger.getLogger(BankClient.class);
     static void getBankApproval() {
         loggerForBankClient.warn("Some Store is trying to charge a money. Do you approve charging? \n1.Yes\n2.No");
        int approving = new Scanner(System.in).nextInt();
        if (approving==1) {
            loggerForBankClient.debug("Payment is finished. Thank you!");
            System.exit(0);
        } else {
            loggerForBankClient.fatal("Sorry, payment was declined. Contact your bank for details. Thanks");
            System.exit(0);
        }
    }
}
