import org.apache.log4j.Logger;

import java.util.Scanner;

class Checkout {

    private static final Logger loggerForCheckout = Logger.getLogger(Checkout.class);

    static void purchase() {
        loggerForCheckout.info("Check your order and address");
        loggerForCheckout.debug("Type the card number");
        checkCardNumber();
        loggerForCheckout.warn("Card is valid. Additional approval in bank client is needed for proceeding");
        BankClient.getBankApproval();
    }

    private static void checkCardNumber() {
        String cardNumber = new Scanner(System.in).next();
        if (cardNumber.length() != 16) {
            loggerForCheckout.error("Please, insert correct card number");
            checkCardNumber();
        }
    }

}
