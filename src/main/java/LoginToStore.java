
import org.apache.log4j.Logger;

import java.util.Scanner;

public class LoginToStore {
    private static final Logger loggerForLogin = Logger.getLogger(LoginToStore.class);
    private static String password = "pass123";

    public static void main(String[] args) {
        loggerForLogin.info("Welcome to our store. To make an order, please login to the system using your email");
        checkEmail();
        for (int i = 0; i < 3; i++) {
            checkPassword();
        }
        loggerForLogin.fatal("Sorry. You are blocked"); //do not work
    }

    private static void checkEmail() {
        String login = new Scanner(System.in).next();
        if (login.isEmpty() || !login.contains("@")) {
            loggerForLogin.error("Please, insert correct email");
            checkEmail();
        } else {
            loggerForLogin.debug("Type your password");
            loggerForLogin.warn("Be aware that if you type incorrect password three times, " +
                    "you will be block for next hour");
        }
    }

    private static void checkPassword() {
            String password = new Scanner(System.in).next();
            if (password.isEmpty()) {
                loggerForLogin.error("Please, insert correct password");
                checkPassword();
            } else if (!LoginToStore.password.equals(password)){
                loggerForLogin.error("Please, insert correct password");
                checkPassword();
            }
            loggerForLogin.debug("Successful");
            Checkout.purchase();
        }
    }


